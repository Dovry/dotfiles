## Login Screen

# [LightDM](https://wiki.archlinux.org/index.php/LightDM#Installation)
Displaymanager
# [Web-greeter](https://github.com/Antergos/web-greeter)
GUI that prompts the user for credentials, lets the user select a session, and so on. 
# [Musfealle](https://github.com/pedropenna/musfealle)
Video(webm) background and HTML/CSS login screen
# [Dots](https://github.com/Dovry/browser-stuff/tree/master/dots)
interactive login screen (you can move the dots and click to make more) it's like a screensaver!
