## Lockscreen

# How it works
install scrot, imagemagick, i3lock

Set your hotkey (Super + L) to run
`location/of/script.sh`
in your DE's keyboard settings

# How it looks

![lockscreen](https://user-images.githubusercontent.com/15201298/31476036-957ac8bc-af04-11e7-982d-7ff4dd56a9ed.png)

you can change the icon to anything you want, just remember to change the filename and location in the script!
